class ArticlesController < ApplicationController
  before_action :set_article, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!
  before_action :validate_user, only: [:edit, :update, :destroy]

  def index
    @articles = Article.all
  end

  def show
  end

  def new
    @article = Article.new
  end

  def edit
  end

  def create
    @article = current_user.articles.new(article_params)
    if @article.save
       redirect_to root_path, notice: 'Article was successfully created.'
    else
     render :new
    end
  end

  def update
    if @article.update(article_params)
      redirect_to root_path, notice: 'Article was successfully updated.'
    else
     render :edit
    end
  end

  def destroy
    @article.destroy
    redirect_to articles_url, notice: 'Article was successfully destroyed.'
  end

  def create_comments
     @comments = current_user.comments.create(:description => params[:description], :article_id => params[:article_id])
     redirect_to article_path(params[:article_id])
  end

  def create_reply
    @reply = current_user.comments.create(:description => params[:description], :article_id => params[:article_id], :reply_id => params[:reply_id])
    redirect_to article_path(params[:article_id])
  end

  def delete_comment
    @comment = Comment.find(params[:comment_id])
    @article = @comment.article
    @comment.destroy
    redirect_to article_path(@article), notice: 'Comment was successfully destroyed.'
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_article
      @article = Article.find(params[:id])
    end

    def validate_user
      redirect_to root_path , notice: 'Access Denied' if current_user.id != @article.user_id
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def article_params
      params.require(:article).permit(:title, :body, :user_id)
    end
end
